# mils_steady_state(αg::T, x::T, y::T, vt::T)
vt = 1e-7
x,y = 3.,2.
α = 0.53*1e-6 #m2s−1
ttest = mils_steady_state(α, x, y, vt)
tss = 8760*3600*1e3
ttest2 = mils(tss, α, x, y, vt)

@time  mils(tss, α, x, y, vt)

print("\nsteady state $ttest ")
print("\nresponse long time $ttest2 ")


tvector = 8760. * 3600. .* [1.,2.,3.,4.,5.]
print("\ntime vector $tvector ")
# ttest3 = mils.(tvector, α, x, y, vt)
ttest3 = mils.(tvector, α, x, y, vt)
# ttest4 = mils(tvector, α, x, y, vt)
print("\nvector response . $ttest3 ")
# print("\nvector response $ttest4")

@time ttest3
# @time ttest4    
# print("\nresponse vector $ttest3 ")



# struct GroundWaterFlow end
# """
# return a vector of mutual distances pairs [(Δri,ΔcosPhii) for i = 1:N] 
# """
# function evaluate_relevant_distances(::GroundWaterFlow, Ps1::Array{T},Ps2::Array{T}) where {T <: Point2}
#     c1 = hcat(collect(Array(p) for p in Ps1)...)
#     c2 = hcat(collect(Array(p) for p in Ps2)...)
#     Δr  = pairwise(Euclidean(),c1,c2)
#     ΔcosPhi = pairwise(CosineDist(),c1,c2)   
#     map(Δr,ΔcosPhi) do r,cosPhi
#         (r,cosPhi)
#     end x    
# end