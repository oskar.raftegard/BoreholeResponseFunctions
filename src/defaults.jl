using Parameters

@with_kw struct Para{R<:Real} @deftype R
    #Grout
    rb = 0.115/2 
    λb = 1.5     
    Cb = 2000. * 1550.          
    αb = λb/Cb	

    # Ground
    λg = 3.
    Cg = 2500. * 750.           
    αg = λg/Cg

    #Pipe Fluid
    # rp = 0.028*sqrt(2)          #equivalent pipe radius
    rp = 0.02*sqrt(2)          #equivalent pipe radius
    λp = 0.42;                  # p - pipe material
    dpw = 0.0023;               # pipe thickness
    hp = 725.;                  # heat transfer coefficient water to pipe ?
    Cw = pi*rp^2 *4182*1000.    # Water capacity

    #Resistances
    Rgrout = 1/(2*pi*λb)*log(rb/rp)                  # grout resistance
    Rp = 1/(2*pi*λp)*log(rp/(rp-dpw)) + 1/(2*pi*rp*hp)   # pipe resistance
    Rb = (Rgrout + Rp)     # in this example the borehole resistance is considered     
end