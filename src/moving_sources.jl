
function mils_steady_state(αg::T, x::T, y::T, vt::T)::T where {T<:AbstractFloat}
    return exp(vt*x/(2αg)) * besselk(0, vt*sqrt(x^2 + y^2)/(2αg))
end

function log_mils_steady_state_r_phi(αg::T, r::T, cosphi::T, vt::T; q = 5., kg = 3., logΔT_threshold = -8)::T where {T<:AbstractFloat}
    return vt*cosphi/(2αg)*r + log( besselk(0, vt*r/(2αg) ) ) + log(q/(2pi*kg)) - logΔT_threshold   
end

function intergrand_mils(ψ,vt,x,y,αg)
    return 1/ψ * exp(-ψ - (vt^2*(x^2 + y^2)/(16*αg^2*ψ)))
end

function mils(t::T, αg::T, x::T, y::T, vt::T; atol=1e-8)::T where {T<:AbstractFloat}
    lb = 0.
    ub = vt^2*t/(4*αg)
    return 1/2*exp(vt*x/(2αg)) * quadgk(ψ -> intergrand_mils(ψ,vt,x,y,αg), lb, ub; atol = atol)[1]
end 

# """
# specific version of this function to optimize it for arrays of time
# """ 
# function mils(t::Array{T,1}, αg::T, x::T, y::T, vt::T; atol = 1e-8)::Array{T,1}  where {T<:AbstractFloat}
#     lb = 0.
#     discretization = vcat(0., vt^2 .*t ./(4*αg))    
#     n = length(discretization)   
#     integrals =  cumsum([quadgk(ψ -> intergrand_mils(ψ,vt,x,y,αg), discretization[k], discretization[k+1]; atol = AbstractFloat)[1] for k=1:n-1])
#     return 1/2*exp(vt*x/(2αg)).*integrals
# end 

function intergrand_mfls(zdummy,t,vt,x,y,z,αg)
    r = sqrt(x^2 + y^2 + (z-zdummy)^2)
    return 1/(4*r)*( exp(-vt*r/(2αg)) * erfc( (r - vt*t)/(2*sqrt(αg*t) ) )  + exp(vt*r/(2αg)) *erfc((r + vt*t)/(2*sqrt(αg*t) ) ) )     
end

function mfls(t::T, αg::T, x::T, y::T, z::T, vt::T, H::T, D::T; atol = 1e-8)::T where {T<:AbstractFloat}
    I1 = quadgk(zdummy -> intergrand_mfls(zdummy,t,vt,x,y,z,αg; atol = atol),  D, H+D)[1]
    I2 = quadgk(zdummy -> intergrand_mfls(zdummy,t,vt,x,y,z,αg; atol = atol), -H-D , -D)[1]
    return exp(vt*x/(2αg)) *(I1 - I2)
end 

function mfls_adiabatic_surface(t::T, αg::T, x::T, y::T, z::T, vt::T, H::T, D::T; atol = 1e-8)::T where {T<:AbstractFloat}
    I1 = quadgk(zdummy -> intergrand_mfls(zdummy,t,vt,x,y,z,αg),  D, H+D; atol = atol)[1]
    I2 = quadgk(zdummy -> intergrand_mfls(zdummy,t,vt,x,y,z,αg), -H-D, -D; atol = atol)[1]
    return exp(vt*x/(2αg)) * (I1 + I2)
end 
